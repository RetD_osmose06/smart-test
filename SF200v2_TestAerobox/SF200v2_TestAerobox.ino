﻿#include <EEPROMex.h>
#include <Controllino.h>

#define ON_OFF_SOUFFLAGE 22

#define CMD_SOUFFLAGE 9
#define CMD_ASPI 13
//#define CMD_REGISTRE 12
#define LED 2
#define PRESS_SOUFFLAGE A14
#define PRESS_ASPI A15
#define FUSIBLE_AEROBOX A2
#define ON_OFF_PERIPH 23
#define CMD_ASPI_PERIPH 8
#define PRESS_ASPI_PERIPH A6
#define COMMUTATEUR 11


						   /*************************************
						   * CLASS FLASHER *
						   * Pour g�rer le blink de la LED*
						   *************************************/
class Flasher
{
	int ledPin;      // the number of the LED pin

					 // These maintain the current state
	int ledState;                 // ledState used to set the LED
	unsigned long previousMillis;   // will store last time LED was updated

									// Constructor - creates a Flasher 
									// and initializes the member variables and state
public:
	Flasher(int pin)
	{
		ledPin = pin;
		pinMode(ledPin, OUTPUT);
		ledState = LOW;
		previousMillis = 0;
	}

	void Update(int mode)
	{
		long OnTime, OffTime;
		switch (mode) {
		case 0:
			OnTime = 0;
			OffTime = 1000;
			break;
		case 1:
			OnTime = 1000;
			OffTime = 0;
			break;
		case 2:
			OnTime = 1000;
			OffTime = 1000;
			break;
		case 3:
			OnTime = 100;
			OffTime = 100;
			break;
		}

		if (OffTime == 0) {
			digitalWrite(ledPin, HIGH);
		}
		else
			if (OnTime == 0) {
				digitalWrite(ledPin, LOW);
			}
			else {
				// check to see if it's time to change the state of the LED
				unsigned long currentMillis = millis();

				if ((ledState == HIGH) && (currentMillis - previousMillis >= OnTime))
				{
					ledState = LOW;  // Turn it off
					previousMillis = currentMillis;  // Remember the time
					digitalWrite(ledPin, ledState);  // Update the actual LED
				}
				else if ((ledState == LOW) && (currentMillis - previousMillis >= OffTime))
				{
					ledState = HIGH;  // turn it on
					previousMillis = currentMillis;   // Remember the time
					digitalWrite(ledPin, ledState);   // Update the actual LED
				}
			}

	}
};

class Ventilo {
public:

	double debit;
	int deltaP;
	int coeffK;
	int consigneDebit;
	int seuilDefaut;
	bool defaut;
	int sortiePWM;
	int hysteresis;

	int mapVoltsMin, mapVoltsMax, mapPressionMin, mapPressionMax, mapPinMin, mapPinMax;


	Ventilo(int _pinPression, int _pinCommande) {
		pinPression = _pinPression;
		pinCommande = _pinCommande;
	};

	void setCapteurPression(int _mapVoltsMin = 50, int _mapVoltsMax = 450, int _mapPressionMin = 0, int _mapPressionMax = 300, int _mapPinMin = 0, int _mapPinMax = 1000) { // Par défaut = capteur 0...300 Pa => 0.5-4.5V sur une pin 0-10V
		mapVoltsMin = _mapVoltsMin;
		mapVoltsMax = _mapVoltsMax;
		mapPressionMin = _mapPressionMin;
		mapPressionMax = _mapPressionMax;
		mapPinMin = _mapPinMin;
		mapPinMax = _mapPinMax;
	}

	int mesurelDeltaP() {
		int input = analogRead(pinPression);
		int val = map(input, 0, 1024, mapPinMin, mapPinMax); //transformation en 0.00 - 5.00V
															 //val = map(val, mapPinMin, mapPinMax, mapVoltsMin, mapVoltsMax);//transformation en 0.50 - 4.500V
															 //Serial.print("0.5-4.5:"); Serial.println(val);
		deltaP = map(val, mapVoltsMin, mapVoltsMax, mapPressionMin, mapPressionMax);//transformation en 0 - 300 Pa
		if (deltaP < 0) deltaP = 0;

	/*	Serial.print("Pts(0-1024):"); Serial.print(input);
		Serial.print("\t0-10v:"); Serial.print((double)val / 100.0); Serial.print(" V");
		Serial.print("\tdeltaP:"); Serial.print((double)deltaP); Serial.println(" Pa");*/

		 Serial.print(input);
		 String str = String(val / 100.0);
		 str.replace('.', ',');
		Serial.print("\t"); Serial.print(str);
		str = String((double)deltaP);
		str.replace('.', ',');
		Serial.print("\t"); Serial.print(str); 

		return deltaP;
	};

	int calculDebit() {
		debit = 0 * debit + 1 * (coeffK * sqrt((double)mesurelDeltaP()));
		return debit;
	};

	bool calculDefaut() {
		defaut = (debit < seuilDefaut);
		return defaut;
	};

	void setParams(int _coeffK, int _consigneDebit, int _seuilDefaut, int _hysteresis) {
		coeffK = _coeffK;
		consigneDebit = _consigneDebit;
		seuilDefaut = _seuilDefaut;
		hysteresis = _hysteresis;
	}

	int regulDebit() {
		//Serial.print("Hysteresis:"); Serial.println(hysteresis);
		calculDebit();
		if (debit < consigneDebit - hysteresis) {
			sortiePWM = sortiePWM + 1;
		}
		if (debit > consigneDebit + hysteresis) {
			sortiePWM = sortiePWM - 1;
		}
		if (sortiePWM <= 0) {
			sortiePWM = 0;
		}
		if (sortiePWM >= 255) {
			sortiePWM = 255;
		}
		//Serial.print("Sortie PWM:"); Serial.println(sortiePWM);
		return sortiePWM;
	}

private:
	int pinPression;
	int pinCommande;
};



/********************************/
/***** VARIABLES GLOBALES *******/
/********************************/
Ventilo Soufflage(PRESS_SOUFFLAGE, CMD_SOUFFLAGE);
Ventilo Aspiration(PRESS_ASPI, CMD_ASPI);
Ventilo Periph(PRESS_ASPI_PERIPH, CMD_ASPI_PERIPH);



void setup() {
	Serial.begin(9600);
	delay(1000);
	delay(500);

	pinMode(ON_OFF_SOUFFLAGE, OUTPUT);
	pinMode(CMD_SOUFFLAGE, OUTPUT);
	pinMode(CMD_ASPI, OUTPUT);
	pinMode(LED, OUTPUT);
	pinMode(PRESS_SOUFFLAGE, INPUT);
	pinMode(PRESS_ASPI, INPUT);
	pinMode(FUSIBLE_AEROBOX, INPUT);
	pinMode(ON_OFF_PERIPH, OUTPUT);
	pinMode(CMD_ASPI_PERIPH, OUTPUT);
	pinMode(PRESS_ASPI_PERIPH, INPUT);
	pinMode(COMMUTATEUR, INPUT);

	digitalWrite(ON_OFF_SOUFFLAGE, LOW);
	digitalWrite(CMD_SOUFFLAGE, LOW);
	digitalWrite(CMD_ASPI, LOW);
	digitalWrite(LED, LOW);
	digitalWrite(ON_OFF_PERIPH, LOW);

		delay(1000);

	int mini_aspi = analogRead(PRESS_ASPI);
	int mini_souff = analogRead(PRESS_SOUFFLAGE);
	int mini_periph = analogRead(PRESS_ASPI_PERIPH);
	//Serial.print("min aspi = "); Serial.println(mini_aspi);
//	Serial.print("min souff = "); Serial.println(mini_souff);

	Soufflage.setCapteurPression((int)(mini_souff*0.97), 450, 0, 300, 0, 1000);
	Soufflage.setParams(2, 20, 15, 5);
	Aspiration.setCapteurPression((int)(mini_aspi*0.97), 450, 0, 300, 0, 1000);
	Aspiration.setParams(42, 500, 450, 10);
	Periph.setCapteurPression((int)(mini_aspi*0.97), 450, 0, 300, 0, 500);
	Periph.setParams(2, 200, 150, 5);

	/*Soufflage.setCapteurPression(50, 450, 0, 300, 0, 1000);
	Soufflage.setParams(2, 14, 10, 1);

	Aspiration.setCapteurPression(50, 450, 0, 300, 0, 1000);
	Aspiration.setParams(9, 52, 40, 1);*/

	if(digitalRead(COMMUTATEUR)) test_aerobox();
	else test_aspiPeriph();
	/*int val_souff = 45;
	int val_aspi = 75;
	Serial.print("TEST SOUFFLAGE = "); Serial.print(val_souff); Serial.println("%");
	Serial.print("TEST ASPI = "); Serial.print(val_aspi); Serial.println("%");
	analogWrite(CMD_ASPI, val_aspi *255 / 100);
	analogWrite(CMD_SOUFFLAGE, val_souff * 255 / 100);
	digitalWrite(ON_OFF_SOUFFLAGE, HIGH);
	delay(10000);
	Serial.print("SOUFFLAGE\t");
	Soufflage.mesurelDeltaP();
	Serial.print("ASPIRATION\t");
	Aspiration.mesurelDeltaP();*/


}




void loop() {
	delay(1);
}

void test_aspiPeriph() {
	Serial.println("TEST ASPI PERIPH");
	Serial.println("\tpoints(0-1024)\t0-10V\tPa");

	Serial.print("TEST A L'ARRET");
	digitalWrite(ON_OFF_PERIPH, LOW);
	delay(1000);
	Serial.print("\t");
	Periph.mesurelDeltaP(); 

	Serial.print("\nTEST A 25%");
	analogWrite(CMD_ASPI_PERIPH, 255 / 4);
	digitalWrite(ON_OFF_PERIPH, HIGH);
	delay(10000);
	Serial.print("\t");
	Periph.mesurelDeltaP();

	Serial.print("\nTEST A 50%");
	analogWrite(CMD_ASPI_PERIPH, 255 / 2);
	digitalWrite(ON_OFF_PERIPH, HIGH);
	delay(10000);
	Serial.print("\t");
	Periph.mesurelDeltaP();

	Serial.print("\nTEST A 75%");
	analogWrite(CMD_ASPI_PERIPH, 3 * 255 / 4);
	digitalWrite(ON_OFF_PERIPH, HIGH);
	delay(10000);
	Serial.print("\t");
	Periph.mesurelDeltaP();

	Serial.print("\nTEST A 100%");
	analogWrite(CMD_ASPI_PERIPH, 255);
	digitalWrite(ON_OFF_PERIPH, HIGH);
	delay(10000);
	Serial.print("\t");
	Periph.mesurelDeltaP();
}

void test_aerobox() {
	Serial.println("TEST AEROBOX");
	Serial.println("\tSOUFFLAGE\t\t\tASPIRATION");
	Serial.println("\tpoints(0-1024)\t0-10V\tPa\tpoints(0-1024)\t0-10V\tPa");

	Serial.print("TEST A L'ARRET");
	analogWrite(CMD_ASPI, 0);
	digitalWrite(ON_OFF_SOUFFLAGE, LOW);
	delay(1000);
	Serial.print("\t");
	Soufflage.mesurelDeltaP(); Serial.print("\t");
	Aspiration.mesurelDeltaP();

	Serial.print("\nTEST A 25%");
	analogWrite(CMD_ASPI, 255 / 4);
	analogWrite(CMD_SOUFFLAGE, 255 / 4);
	digitalWrite(ON_OFF_SOUFFLAGE, HIGH);
	delay(10000);
	Serial.print("\t");
	Soufflage.mesurelDeltaP(); Serial.print("\t");
	Aspiration.mesurelDeltaP();

	Serial.print("\nTEST A 50%");
	analogWrite(CMD_ASPI, 255 / 2);
	analogWrite(CMD_SOUFFLAGE, 255 / 2);
	digitalWrite(ON_OFF_SOUFFLAGE, HIGH);
	delay(10000);
	Serial.print("\t");
	Soufflage.mesurelDeltaP(); Serial.print("\t");
	Aspiration.mesurelDeltaP();

	Serial.print("\nTEST A 75%");
	analogWrite(CMD_ASPI, 3 * 255 / 4);
	analogWrite(CMD_SOUFFLAGE, 3 * 255 / 4);
	digitalWrite(ON_OFF_SOUFFLAGE, HIGH);
	delay(10000);
	Serial.print("\t");
	Soufflage.mesurelDeltaP(); Serial.print("\t");
	Aspiration.mesurelDeltaP();

	Serial.print("\nTEST A 100%");
	analogWrite(CMD_ASPI, 255);
	analogWrite(CMD_SOUFFLAGE, 255);
	digitalWrite(ON_OFF_SOUFFLAGE, HIGH);
	delay(10000);
	Serial.print("\t");
	Soufflage.mesurelDeltaP(); Serial.print("\t");
	Aspiration.mesurelDeltaP();
}



bool elapsed(unsigned long *previousMillis, unsigned long interval) {
	if (*previousMillis == 0) {
		*previousMillis = millis();
	}
	else {
		if ((unsigned long)(millis() - *previousMillis) >= interval) {
			*previousMillis = 0;
			return true;
		}
	}
	return false;
}




